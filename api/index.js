import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
// import models from './server/src/models';
import taskRoutes from './server/src/routes/TaskRoutes';
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
const port = process.env.PORT || 8000;

app.use(cors());
// when a random route is inputed
app.get('/', (req, res) => res.status(200).send({
   message: 'Welcome to this API.'
}));


app.use('/api/taskList', taskRoutes);

app.listen(port, () => {
   console.log(`Server is running on PORT ${port}`);
});

// Sequelize.sync().then(function () {
//     app.listen(port);
//     app.on('error', 'Error');
//     app.on('listening', onListening);
//   });
export default app;