import database from '../models';
// import Task from '../models/task;'
// import TaskList from '../models/task;'

class TaskService {
    static async getAllTaskListWithSubTask() {
      try {
        return await database.TaskList.findAll({include: [
            {model: database.Task, as: 'tasks'}]});
      } catch (error) {
        throw error;
      }
    }

    static async createTaskList(newTaskList) {
        try {
          return await database.TaskList.create(newTaskList);
        } catch (error) {
          throw error;
        }
    }

    static async getTaskListById(id) {
        try {
          const task = await database.TaskList.findOne(
                { 
                    include: [ {
                      model: database.Task,
                      as: 'tasks'
                    }],
                    
                }, 
                {
                    where: {
                        id: id,
                    }
                });
          return task;
        } catch (error) {
          throw error;
        }
    }

    static async deleteTaskListById(id) {
        try {
          return await database.TaskList.destroy(
                {
                    where: {
                        id: +id,
                    },
                    force: true, // delete row if model paranoid
                });
        } catch (error) {
          throw error;
        }
    }

    static async createTask(task, id) {
      try {
        Object.assign(task, {task_list_id: +id});
        return await database.Task.create(task);
      } catch (error) {
        throw error;
      }
    }

    static async removeTaskById(id) {
      try {
        return await database.Task.destroy({where: {id: +id}});
      } catch (error) {
        throw error;
      }
    }

    static async completeTaskById(id) {
      try {
        return await database.Task.update({is_finished: true}, {where: {id: +id}});
      } catch (error) {
        throw error;
      }
    }

    static async uapdateTaskById(id, updatedTask) {
      try {
        const task = await Task.findOne({where: {id: +id}})
        if (task) {
          task.is_finished = updatedTask.is_finished;
          task.name = updatedTask.name;
          await task.save();
        } else {
          return false;
        }
        
      } catch (error) {
        throw error;
      }
    }
}

export default TaskService;