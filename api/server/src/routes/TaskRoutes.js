import { Router } from 'express';
import TaskController from '../controllers/controllers/TaskController';

const router = Router();


router.get('/', async function(req, res) {
    await TaskController.getAllTask(req, res);
    // res.send(res);
  });

router.get('/:id', async function(req, res) {
    await TaskController.getTaskListByid(req, res);
    // res.send(res);
});
router.post('/', async function(req, res) { 
    await TaskController.createTaskList(req, res);
});

router.post('/:id/task', async function(req, res) { 
  await TaskController.createTask(req, res);
});

router.delete('/task/:id', async function(req, res) { 
  await TaskController.removeTask(req, res);
});

router.post('/task/:id/complete', async function(req, res) { 
  await TaskController.completeTask(req, res);
});

router.post('/task/:id/update', async function(req, res) { 
  await TaskController.updatedTask(req, res);
});

export default router;