import TaskService from '../../services/TaskService';
import Util from '../../utils/Util';

const util = new Util();

class TaskController {
  // method for return all task list with tasks
  static async getAllTask(req, res) {
    try {
      const allListTasks = await TaskService.getAllTaskListWithSubTask();
      if (allListTasks.length > 0) {
        // util.setSuccess(200, 'Task retrieved', allListTasks);
        res.send(allListTasks);
      } else {
        res.send([])
      }
      return res.send(allListTasks);
    } catch (error) {
      console.log(error);
      util.setError(400, error);
      return util.send(res);
    }
  }

  // method for create task list
  // @param {body: string} title of task list
  static async createTaskList(req, res) {
    if (!req.body.name) {
      util.setError(400, 'Please provide complete details');
      return util.send(res);
    }
    const newTaskList = req.body;
    try {
      const createdTaskList = await TaskService.createTaskList(newTaskList);
      util.setSuccess(201, 'Task List Added!', createdTaskList);
      return util.send(res);
    } catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  }


  // return task list by id
  static async getTaskListByid(req, res) {
    try {
      const { id } = req.params;
      if (!Number(id)) {
        util.setError(400, 'Please input a valid numeric value');
        return util.send(res);
      }
      const taskList = await TaskService.getTaskListById(id);
      if (taskList.length > 0) {
        util.setSuccess(200, 'Task retrieved', taskList);
      } else {
        util.setSuccess(200, 'No task found');
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }

  // create task in task list
  // @param {id: number} id of taskList
  // @param {body: string} title of task, default is_finished: fasle
  static async createTask(req, res) {
    try {
      const { id } = req.params;
      if (!Number(id)) {
        util.setError(400, 'Please input a valid numeric value');
        return util.send(res);
      }
      const taskList = await TaskService.getTaskListById(id);
      if (taskList) {
        const createTask = await TaskService.createTask(req.body, id)
        util.setSuccess(200, 'Task retrieved', createTask);
      } else {
        util.setSuccess(200, 'No task found');
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }

  // remove task by id
  // @param {id: number}
  static async removeTask(req, res) {
    const { id } = req.params;
      if (!Number(id)) {
        util.setError(400, 'Please provide a numeric value');
        return util.send(res);
      }
    try {
        const taskToDelete = await TaskService.removeTaskById(id);
            if (taskToDelete) {
              util.setSuccess(200, 'Task deleted');
            } else {
              util.setError(404, `Task with the id ${id} cannot be found`);
            }
            return util.send(res);
          } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    }


    //complete task by id, set is_finished: true
    // @param {id: number}
    static async completeTask(req, res) {
      const { id } = req.params;
        if (!Number(id)) {
          util.setError(400, 'Please provide a numeric value');
          return util.send(res);
        }
      try {
          const taskCompleted = await TaskService.completeTaskById(id);
              if (taskCompleted) {
                util.setSuccess(200, 'Task completed');
              } else {
                util.setError(404, `Task with the id ${id} cannot be found`);
              }
              return util.send(res);
            } catch (error) {
              util.setError(400, error);
              return util.send(res);
          }
      }
    
  
  // update task
  static async updatedTask(req, res) {
    const alteredTask = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }
    try {
      const updateTask = await TaskService.updateTaskById(id, alteredTask);
      if (!updateBook) {
        util.setError(404, `Cannot find Task with the id: ${id}`);
      } else {
        util.setSuccess(200, 'Task updated', updateBook);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }
}

export default TaskController;