'use strict';
module.exports = (sequelize, DataTypes) => {
  const Task = sequelize.define('Task', {
    id: { 
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
    },
    name: DataTypes.STRING,
    is_finished: DataTypes.BOOLEAN,
    // task_list_id: {
    //   type: DataTypes.INTEGER,
    //   references: {
    //     model: 'TaskList', // name of Target model
    //     key: 'id', // key in Target model that we're referencing
    //   },
    //   onUpdate: 'CASCADE',
    //   onDelete: 'SET NULL',
    // },
  }, {
    classMethods: {
      associate: (models) => {
        Task.belongsTo(models.TaskList, { foreignKey: "task_list_id" });
      }
    }
  });
  // Task.associate = function(models) {
  //   models.Task.belongsTo(models.TaskList, {
  //       foreignKey: 'task_list_id'
  //   });
  
  // }
  Task.associate = function(models) {
    Task.belongsTo(models.TaskList, {
      foreignKey: 'task_list_id'
    });
  };
  // Task.belongsTo(models.TaskList);
  return Task;
};