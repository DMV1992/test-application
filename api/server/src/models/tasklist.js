'use strict';
module.exports = (sequelize, DataTypes) => {
  const TaskList = sequelize.define('TaskList', {
    id:  { 
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: DataTypes.STRING
  }, {
  //   classMethods: {
  //     associate: function(models) {
  //       TaskList.hasMany( models.Task, {
  //         foreignKey: 'task_list_id',
  //         sourceKey: 'task_list_id'
  //       } );
  //     }
  // }
  });

  TaskList.associate = models => {
    TaskList.hasMany(models.Task, {foreignKey: 'task_list_id', as: 'tasks'});
  };

  // TaskList.associate = function(models) {
  //   // associations can be defined here
  //   models.TaskList.hasMany(models.Task, {
  //     foreignKey: 'task_list_id',
  //     // as: 'tasks'
  //   });
  // };
  // TaskList.hasMany(Task);
  return TaskList;
};