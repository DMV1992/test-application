# Test app

## Installation
First step 
```bash
npm install
```
Second
Setup .ENV
```bash 
DB_NAME

DB_USER

DB_PASS

DB_PORT

DB_HOST

SECRET_KEY
```
Third
```bash
npm run db:migrate
```

# app

## Description

test application

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

```
## Endpoint

  Returns Task list models
* **URL**
  /api/taskList
* **Method:**
  `Get` 


    Return Task List by id with task
* **URL**
  /api/taskList/:id
* **Method:**
  `Get`


    Crate Task List
* **URL**
  /api/taskList/
* **Method:**
  `Post`
* **REQ**
    {name: task list name : string}


    Crate Task List
* **URL**
   /api/taskList/:id/task
* **Method:**
  `Post`
* **Params:**
    id: taskList id
* **REQ**
    {name: task name : string}

    Delete Task
* **URL**
   /api/taskList/task:id
* **Method:**
  `Delete`
* **Params:**
    id: task id

    Complete Task
* **URL**
   /api/taskList/task:id
* **Method:**
  `Post`
* **Params:**
    id: task id

     Update Task
* **URL**
   /api/taskList/task:id
* **Method:**
  `Post`
* **Params:**
    id: task id
* **REQ**
    {name: task name : string}
